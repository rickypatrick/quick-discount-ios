//
//  ViewController.swift
//  QuickDiscount
//
//  Created by Ricky Patrick on 6/17/17.
//  Copyright © 2017 Ricky Patrick. All rights reserved.
//

import UIKit
import GoogleMobileAds




private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}

extension String
{
    func safelyLimitedTo(length n: Int)->String {
        let c = self.characters
        if (c.count <= n) { return self }
        return String( Array(c).prefix(upTo: n) )
    }
}
class ViewController: UIViewController {
    
    
    
    
    
    
    
    
    
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var priceSlider: UISliderDesignable!
    @IBOutlet weak var discount1Slider: UISlider!
    @IBOutlet weak var discount2Slider: UISlider!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var discount1Label: UILabel!
    @IBOutlet weak var discount2Label: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var savedLabel: UILabel!
    
  
    @IBOutlet weak var testBackground: UIImageView!
    public var discount2BackgroundAlpha: CGFloat = 0.00
    
    public var value = 0
    public var discount1 = 0
    public var discount2 = 0
    public var discountedPrice = 0.00
    public var discount1Percent = 0.00
    public var discount2Percent = 0.00
    public var discountAmount = 0.00
    public var taxPercentage = 0.00
    public var taxAmount = 0.00
    
    
    var bannerView: GADBannerView!
    
    let defaults = UserDefaults.standard
    
   
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        bannerView = GADBannerView(adSize: kGADAdSizeFullBanner)
        self.view.addSubview(bannerView)
        bannerView.adUnitID = "ca-app-pub-7923031414188595/2358354634"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print("Max Price was set to \(String(describing: maxPriceAmt))")
        if let priceMaximum = maxPriceAmt {
            print("Contains a value! It is \(priceMaximum)!")
   //         priceSlider.maximumValue = Float(priceMaximum)
            let savedMaxPrice = defaults.integer(forKey: "maxPrice")
            
            if savedMaxPrice == 0 {
                priceSlider.maximumValue = 100
            } else {
            priceSlider.maximumValue = Float(savedMaxPrice)
            }
            
            
                    } else {
            print("Doesn’t contain a number")
        }
        var taxes = defaults.integer(forKey: "taxPercent")
        taxPercentage = Double(taxes) * 0.01
        
        print("Tax percent is setto \(taxPercentage)")
        
        if taxPercentage > 0 {
            print("We need to show the taxes")
            
        }
        else {
            taxLabel.isHidden = true
        }
        totalLabel.text = ""
        savedLabel.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sliderMoved(_ slider: UISlider){
//       value = lroundf(slider.value)
        value = Int(slider.value)
 //       var valueLabel = lroundf(slider.value)
        print("Slider value is \(value)")
        priceLabel.text = String(value)
        
        
        //Does the Math whenever the slider is moved
        discountedPrice = Double(value) - (Double(value) * discount1Percent)
        taxAmount = discountedPrice * taxPercentage
        discountedPrice = discountedPrice - (discountedPrice * discount2Percent) + taxAmount
        
        
        print("Discounted Price is \(discountedPrice)")
        round(discountedPrice)
        totalLabel.text = ("$") + String(format:"%.2f", discountedPrice)
        
        
        //End of math
        

        
        
        if discountedPrice < Double(value) {
            savedLabel.isHidden = false
            discountAmount = Double(value) - discountedPrice
            savedLabel.text = ("You saved $") + String(format:"%.2f",discountAmount)
        }
        else {
            savedLabel.isHidden = true    }

        
        
        if taxPercentage <= 0 {
            taxLabel.isHidden = true
        }
        else {
            taxLabel.isHidden = false
            taxLabel.text = ("Price includes $") + String(format:"%.2f",taxAmount) + (" in taxes")
        }
        }
    
    
    
        
    
    
    @IBAction func discountSliderMoved(_ slider: UISlider){
        //       value = lroundf(slider.value)
        discount1 = Int(slider.value)
        //       var valueLabel = lroundf(slider.value)
        print("Discount 1 Slider value is \(discount1)")
        discount1Label.text = String(discount1)
        
        discount1Percent = Double(discount1) * 0.01
        print("Discount 1 is \(discount1Percent) %")
        
        //Does the Math whenever the slider is moved
        discountedPrice = Double(value) - (Double(value) * discount1Percent)
        taxAmount = discountedPrice * taxPercentage
        discountedPrice = discountedPrice - (discountedPrice * discount2Percent) + taxAmount

        print("Discounted Price is \(discountedPrice)")
        round(discountedPrice)
        totalLabel.text = ("$") + String(format:"%.2f", discountedPrice)
        //End of math
        
        //Updates savings label at the top
        if discountedPrice < Double(value) {
            savedLabel.isHidden = false
            discountAmount = Double(value) - discountedPrice
            savedLabel.text = ("You saved $") + String(format:"%.2f",discountAmount)
                    }
        else {
            savedLabel.isHidden = true    }
        
        if taxPercentage <= 0 {
            taxLabel.isHidden = true
        }
        else {
            taxLabel.isHidden = false
            taxLabel.text = ("Price includes $") + String(format:"%.2f",taxAmount) + (" in taxes")
        }

        
    }
    
    @IBAction func discount2SliderMoved(_ slider: UISlider){
        //       value = lroundf(slider.value)
        discount2 = Int(slider.value)
        //       var valueLabel = lroundf(slider.value)
        print("Discount 2 Slider value is \(discount2)")
        discount2Label.text = String(discount2)
        
        discount2Percent = Double(discount2) * 0.01
        print("Discount 2 is \(discount2Percent) %")
      
        
        //Does the Math whenever the slider is moved
        discountedPrice = Double(value) - (Double(value) * discount1Percent)
        discountedPrice = discountedPrice - (discountedPrice * discount2Percent)
        taxAmount = discountedPrice * taxPercentage
        discountedPrice = discountedPrice + taxAmount
            print("Discounted Price is \(discountedPrice)")
        
        totalLabel.text = ("$") + String(format:"%.2f", discountedPrice)
        //End of math
        
        
        
        //Updates savings label at the top
        if discountedPrice < Double(value) {
            savedLabel.isHidden = false
            discountAmount = Double(value) - discountedPrice
            savedLabel.text = ("You saved $") + String(format:"%.2f",discountAmount)
    } else {
    savedLabel.isHidden = true    }
        if taxPercentage <= 0 {
            taxLabel.isHidden = true
        }
        else {
            taxLabel.isHidden = false
            taxLabel.text = ("Price includes $") + String(format:"%.2f",taxAmount) + (" in taxes")
        }


    }
 

    }



