//
//  SettingsViewController.swift
//  QuickDiscount
//
//  Created by Ricky Patrick on 7/4/17.
//  Copyright © 2017 Ricky Patrick. All rights reserved.
//

import UIKit

var maxPriceAmt : Int? = 100
var taxAmt = 0

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var maxPrice : UITextField!
    
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var taxButton: UISwitch!
    
    @IBOutlet weak var taxField: UITextField!
    let defaults = UserDefaults.standard
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
  //Add done button to close the keyboard
        
        
        
        let shouldTaxesShow = defaults.bool(forKey: "showTaxes")
        
        let savedMaxPrice = defaults.integer(forKey: "maxPrice")
        let savedTaxAmt = defaults.integer(forKey: "taxPercent")
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        
        toolBar.setItems([doneButton], animated: true)
        
        maxPrice.inputAccessoryView = toolBar
        taxField.inputAccessoryView = toolBar
        
        
        maxPrice.text = String(savedMaxPrice)
        taxField.text = String(savedTaxAmt)
        
        if shouldTaxesShow == true {
            print("the taxes should show")
            taxButton.setOn(true, animated: true)
            taxLabel.isHidden = false
            taxField.isHidden = false
            
        }
    
    }

    func doneClicked() {
        
        view.endEditing(true)
        
        if let text = maxPrice.text, !text.isEmpty
        {
            
            maxPriceAmt = (Int(maxPrice.text!))!
            
            UserDefaults.standard.set(maxPrice.text, forKey: "maxPrice")
            
            
             
        }
        
        if let text = taxField.text, !text.isEmpty
        {
            
            taxAmt = (Int(taxField.text!))!
            
            UserDefaults.standard.set(taxField.text, forKey: "taxPercent")
            
            print("Taxes have been set to \(taxAmt)")
            
        }
       
    }
  
 
    
    @IBAction func taxButtonToggled(_ sender: UISwitch) {
        if taxButton.isOn {
            print("Taxes turned on")
            taxLabel.isHidden = false
            taxField.isHidden = false
            UserDefaults.standard.set(true, forKey: "showTaxes")
            
            
        } else {
            print("Taxes turned off")
            taxLabel.isHidden = true
            taxField.isHidden = true
            UserDefaults.standard.set(0, forKey: "taxPercent")
            UserDefaults.standard.set(false, forKey: "showTaxes")
        }
    }
    
    
    
    //this is the fuction that is called when the Done button is clicked. This closes the keyboard

    
    @IBAction func close() {
        
        dismiss(animated: true, completion:nil)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
