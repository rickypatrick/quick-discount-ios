//
//  UISliderDesignable.swift
//  QuickDiscount
//
//  Created by Ricky Patrick on 7/2/17.
//  Copyright © 2017 Ricky Patrick. All rights reserved.
//

import UIKit

class UISliderDesignable: UISlider {

    @IBInspectable var thumbImage: UIImage? {
        didSet {
            setThumbImage(thumbImage, for: .normal)
        }
    }
    @IBInspectable var height: CGFloat = 2
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: bounds.origin, size: CGSize(width: bounds.width, height: height))
    }
    
}


